# Lightweight email Notification client

Project Website: [git.velleto.com/gavel](http://git.velleto.com/gavel).

This project is **_work in progress_**.  
Currently there is no stable version available.

### Contributing

There is no `develop` branch in this repository. Feature branches are directly opened from master, and then merged back in.  
Release worthy versions will be cherry-picked onto the release branch, packaged with python and pushed to [PyPI](https://pypi.org/) so they can be installed with `pip`.
