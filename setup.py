import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="gavel",
    version="0.0.1",
    author="Ryan Rueger",
    author_email="dev@velleto.com",
    description="A lightweight email notification client.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="http://git.velleto.com/gavel",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=["IMAPClient==2.0.0"],
    scripts=["bin/gavel"],
)
