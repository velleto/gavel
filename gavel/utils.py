import os
import re


def keychain(path):
    """Return passphrase from GPG encrypted passphrase file."""

    cmd = "gpg -dq {}".format(path)

    try:
        return os.popen(cmd).read().strip()

    except subprocess.CalledProcessError as err:
        print("gavel: ERROR: Reading password: {}".format(err))


def get_contacts(path):
    """Parse a mutt aliases file to return contact information.

    Reminder: A valid mutt alias file line has the following format

        alias <nickname> [ <long name> ] <<address>>
    """

    regex = r"[\-\.+_a-zA-Z0-9]+@[\-\.a-zA-Z0-9]+"
    contacts = {}
    try:
        with open(path, "r") as file:
            for line in file:
                line = line.split()
                # Although legal for a mutt alias, we ignore lines with fewer
                # than 4 entries as they do not contain what is refered to by
                # the mutt manual as a 'long name'. This is likely what we want
                # to see in our notification.
                if len(line) > 3:
                    email = line[-1]
                    email = re.findall(regex, email)[0]
                    name = " ".join(line[2:-1])
                    contacts[email] = name

        return contacts

    except FileNotFoundError:
        return None


def get_name(sender, contacts):
    """Parse 'from' header from incoming emails, return name from contacts."""

    regex = r"[\-\.+_a-zA-Z0-9]+@[\-\.a-zA-Z0-9]+"

    email = re.findall(regex, sender)[0]
    name = contacts.get(email, False)

    if name:
        return name

    return sender
