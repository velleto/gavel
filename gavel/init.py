import os
import sys
import email
import argparse
import imapclient
import configparser
import datetime as dt

import gavel.utils
import gavel.grexit
import gavel.notify


class Gavel():

    def run(self):
        """Invoke everything."""

        self.args = self.__parse_options()
        self.__parse_config()
        self.password = gavel.utils.keychain(self.passpath)
        if self.contacts:
            self.contacts = gavel.utils.get_contacts(self.contacts)

        gavel.grexit.Grexit(self.__leave)

        while True:
            self.__idle()

    def __parse_options(self):
        """Parse command line options."""
        parser = argparse.ArgumentParser()

        parser.add_argument('cnf',
                            metavar="CONFIG",
                            nargs="?",
                            default="~/.gavelrc",
                            help="Specify path to the root config file. "
                                 "Default is ~/.gavelrc")

        args = parser.parse_args()
        args.cnf = os.path.expanduser(args.cnf)

        return args

    def __parse_config(self):
        """Parse configuration file."""
        try:
            config = configparser.ConfigParser()

            if not config.read(self.args.cnf):
                print("gavel: ERROR: Could not read configuration file: \"{}\""
                      .format(self.args.cnf))
                sys.exit(1)

            credentials = config['Credentials']

            # Required configurations.
            # Raise KeyError (purposely) if not present, and handle below with
            # except statements.
            self.hostname = credentials['hostname']
            self.username = credentials['username']
            self.passpath = credentials['passpath']

            # This option is optional, so we include a fallback value.
            self.contacts = credentials.get('contacts', '')
            self.contacts = os.path.expanduser(self.contacts)

        except KeyError as err:

            # To ensure more helpful error messages we separate the cases.
            if err == 'Credentials':
                print("gavel: ERROR: Reading configuration file \"{}\". "
                      "Could not find \'Credentials\' section."
                      .format(self.args.cnf))
            else:
                print("gavel: ERROR: Reading configuration file \"{}\". "
                      "Could not find option \"{}\"."
                      .format(self.args.cnf, err))

            sys.exit(1)

    def __idle(self):
        """Idle the server for 10 minutes and send notifications on new mail.

        Although RFC-2177 states that IDLE connections should be reissued at
        least every 29 minutes, the imapclient documentation suggests a 10
        minute period.
        """

        self.server = imapclient.IMAPClient(self.hostname)
        self.server.login(self.username, self.password)
        self.server.select_folder('INBOX', readonly=True)
        self.server.idle()

        end = dt.datetime.now() + dt.timedelta(minutes=10)
        while dt.datetime.now() < end:

            response = self.server.idle_check(timeout=10)
            if self.__isrec(response):

                self.server.idle_done()
                uid = self.__latest()
                sender, subj = self.__fetch(uid)

                if self.contacts:
                    sender = gavel.utils.get_name(sender, self.contacts)

                note = gavel.notify.Notification()
                note.send(sender, subj)
                self.server.idle()

        self.server.idle_done()
        self.server.logout()

    def __latest(self):
        """Return UID of latest message on server."""

        uid = self.server.search('ALL')[-1]
        return uid

    def __isrec(self, responses):
        """Check whether IMAP response has a RECENT attribute.

        In IDLE mode, the server sends various response codes.
        When new mail arrives a (<n>, b'RECENT') tuple.
        """

        for n, *response in responses:
            if response == [b'RECENT']:
                return True
        return False

    def __fetch(self, uid):
        """Fetch latest message for sender and summary.

        The whole message is downloaded, parsed as an email and the
        subject and sender extracted. The IMAP protocol doesn't allow
        for only checking for the headers.
        """

        data = self.server.fetch(uid, 'RFC822').get(uid)
        msg = email.message_from_bytes(data[b'RFC822'])

        sender = msg.get('From')
        subject = msg.get('Subject')

        return sender, subject

    def __leave(self, SIGNUM, FRAME):

        print("gavel: KILLED.")
        sys.exit()
