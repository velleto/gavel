import gi
gi.require_version('Notify', '0.7')

from gi.repository import GObject
from gi.repository import Notify


class Notification(GObject.Object):

    def __init__(self):

        super(Notification, self).__init__()
        Notify.init("Notification")

    def send(self, title, text="", icon=""):

        note = Notify.Notification.new(title, text, icon)
        note.show()
