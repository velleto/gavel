import signal


class Grexit():

    def __init__(self, quitter):
        signal.signal(signal.SIGINT, quitter)
        signal.signal(signal.SIGTERM, quitter)
